Git Cheat Sheet
=============

A git cheat sheet with shortcuts, answers to questions and other fun facts I've learnt over the years.

## Setup

##### Initializing a local repo in the current directory
```
git init
```

##### List configuration parameters
```
git config --list
```

##### Get a particular configuration parameter
```
git config --get [parameter, e.g. user.name]
```

##### Set a particular configuration parameter
```
git config --[global|system|local] [parameter] [value]
```

## Aliases
##### Create an alias
```
git config --[global|system|local] alias.[alias name] '[command between quotes]'
```
Example : ``` git config --global alias.co 'checkout'```

##### Show existing aliases
```
git config --get-regexp alias
```

##### Useful aliases
Formatting the output of `git log` to make it more readable (and useful):
```
git config --global alias.lol "log --all --decorate --oneline --graph --format=format:'%h - %C(bold green)(%ar)%C(reset) %s - %C(dim white)%an%C(reset) %C(bold yellow)%d%C(reset)'"
```

Pull a repo and initialise its submodules (use `!` to tell git that multiple commands are coming, and `&&` to chain them):
```
git config --global alias.spull "!git pull && git submodule sync --recursive && git submodule update --init --recursive"
```

Create an alias to show all aliases:
```
git config --global alias.alias "config --global --get-regexp alias"
```

## Cloning a repository

### Cloning a simple repository from a URL

##### Cloning a repository
```
git clone [url]
```

##### Cloning to a specific location
```
git clone [url] [location]
```

##### Cloning a repository with submodules

This command clones the main repository then fetches all submodules.
```
git clone [url] [location] --recursive
```

### Managing submodules

##### Initialise git submodules

After cloning the repo, run this command to initialize the submodules in the configuration:
```
git submodule init
```

##### Add git submodule
```
git submodule add [path/to/repo] [destination/folder]
```

##### Add git submodule with default branch
```
git submodule add -b [branch] [path/to/repo] [destination/folder]
```

##### Get submodules
Remove the `--recursive` option if the submodules are located in the repo's root folder.
```
git submodule update --recursive
```

##### Initialise and get at the same time
```
git submodule update --init --recursive
```

##### Update the submodules' remotes if they have changed in the main repo
```
git submodule sync
```

## Managing remote repositories

##### Viewing the repository's remotes

```
git remote -v
```
##### Adding a remote
```
git remote add [name] [url]
```

##### Renaming a remote
```
git remote rename [old] [new]
```

##### Removing a remote
```
git remote remove [name]
```
